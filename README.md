# StudyHelper Profiling Tools

Tools required to profile [StudyHelper Bot](https://gitlab.com/advprog.c12/study-helper-bot) and [StudyHelper Tools](https://gitlab.com/advprog.c12/studyhelper-tools).

### Cloning this repository

```shell
git clone https://gitlab.com/advprog.c12/studyhelper-profiling.git
```

## A. Profiling StudyHelper Tools

### 1. Navigate to `studyhelper-tools`

```shell
cd studyhelper-tools
```

### 2. Run the Application

Run the Application via IntelliJ IDEA using Java Profiler. Shortcut can be found under `Run` > `Profile 'StudyHelperToolsApplication'`.

> **Note**: You need to have the Jrofiler IntelliJ Plugin installed. To install, go to `File` > `Settings` then `Plugins`.

### 3. Populate the database

```shell
javac Populate.java
java Populate
```

### 4. Run the **Locust** file

On **Windows**:

```bat
run
```

On **Mac/Linux**:

```shell
bash run.sh
```

### 5. Open the Locust Dashboard

The Locust Dashboard can be accesed through: `http://localhost:8089/`
