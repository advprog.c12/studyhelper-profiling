from locust import HttpUser, task, between

import random

class StressTest(HttpUser):

    wait_time = between(1, 5)

    '''
    Calculator Stress Test
    '''
    # Arithmetic
    @task
    def add(self):
        self.client.get(url=f"/calculator/arithmetic/add/10/9")

    @task
    def sub(self):
        self.client.get(url=f"/calculator/arithmetic/sub/10/9")

    @task
    def mul(self):
        self.client.get(url=f"/calculator/arithmetic/mul/10/9")

    @task
    def div(self):
        self.client.get(url=f"/calculator/arithmetic/div/10/2")

    @task
    def mod(self):
        self.client.get(url=f"/calculator/arithmetic/mod/10/9")


    # Special
    @task
    def cndf(self):
        self.client.get(url=f"/calculator/special/cndf/90")

    @task
    def to_binary(self):
        self.client.get(url=f"/calculator/special/toBinary/19")

    @task
    def to_decimal(self):
        self.client.get(url=f"/calculator/special/toDecimal/1010")


    # Statistics
    @task
    def mean(self):
        self.client.get(url=f"/calculator/statistics/mean/?values=1,2,2,2,3")

    @task
    def median(self):
        self.client.get(url=f"/calculator/statistics/median/?values=1,2,2,2,3")

    @task
    def mode(self):
        self.client.get(url=f"/calculator/statistics/mode/?values=1,2,2,2,3")


    # Trigonometry
    @task
    def csc(self):
        self.client.get(url=f"/calculator/trigonometry/csc/45")

    @task
    def cos(self):
        self.client.get(url=f"/calculator/trigonometry/cos/45")

    @task
    def cot(self):
        self.client.get(url=f"/calculator/trigonometry/cot/45")

    @task
    def sec(self):
        self.client.get(url=f"/calculator/trigonometry/sec/45")

    @task
    def sin(self):
        self.client.get(url=f"/calculator/trigonometry/sin/45")

    @task
    def tan(self):
        self.client.get(url=f"/calculator/trigonometry/tan/45")

    @task
    def to_degree(self):
        self.client.get(url=f"/calculator/trigonometry/toDegree/45")
    
    @task
    def to_rad(self):
        self.client.get(url=f"/calculator/trigonometry/toRad/45")



    '''
    Sample Stress Test
    '''
    @task
    def get_sample(self):
        sample_id = str(random.randint(0, 5))
        self.client.get(url=f"/sample/ID-0{sample_id}", name="Sample GET")
    
    @task
    def post_sample(self):
        sample_id = str(random.randint(0, 5))

        payload = {
            "sampleId": f"ID-0{sample_id}",
            "dataString": f"This is data number {sample_id}"
        }

        self.client.post(url="/sample", json=payload, name="Sample POST")

    

    '''
    Randomizer Stress Test
    '''
    @task
    def group(self):
        number = str(random.randint(1, 7))
        self.client.get(url=f"/randomizer/grouping/group/{number}", name="Randomizer Group")
    
    @task
    def group_file(self):
        number = str(random.randint(1, 7))
        self.client.get(url=f"/randomizer/groupingfile/group/{number}", name="Randomizer Group File")

    @task
    def groupby(self):
        number = str(random.randint(1, 7))
        self.client.get(url=f"/randomizer/grouping/groupby/{number}", name="Randomizer GroupBy")
    
    @task
    def groupby_file(self):
        number = str(random.randint(1, 7))
        self.client.get(url=f"/randomizer/groupingfile/groupby/{number}", name="Randomizer GroupBy File")

    @task
    def show_list(self):
        self.client.get(url="/randomizer/showlist", name="Randomizer Show List")
