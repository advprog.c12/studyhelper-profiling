import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Populate {
    private static final String URL = "http://localhost:8080";

    public static void main(String[] args) {
        populateSample();
        populateRandomizer();
    }

    public static void populateSample() {
        for (int i = 0; i <=5; i++) {
            String sampleId = String.format("ID-0%s", Integer.toString(i));
            String dataString = String.format("This is data number %s",  Integer.toString(i));

            String jsonString = String.format("{\n"
                + "    \"sampleId\": \"%s\",\n"
                + "    \"dataString\": \"%s\"\n"
                + "}", sampleId, dataString);
            
            try {
                sendPostRequest(URL + "/sample", jsonString);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void populateRandomizer() {
        List<String> values = new ArrayList<>();

        for (int ascii = 65; ascii <= 90; ascii++) {
            values.add(Character.toString((char) ascii));
        }

        for (int ascii = 97; ascii <= 122; ascii++) {
            values.add(Character.toString((char) ascii));
        }

        try {
            sendGetRequest(URL + "/randomizer/register/?values=" + String.join(",", values));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String sendGetRequest(String requestUrl) throws Exception {
        URL urlForGetRequest = new URL(requestUrl);
        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();

        connection.setRequestMethod("GET");

        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuffer jsonString = new StringBuffer();

        String line;
        while ((line = br.readLine()) != null) {
            jsonString.append(line);
        }
        br.close();

        connection.disconnect();

        return jsonString.toString();
    }

    public static String sendPostRequest(String requestUrl, String payload) throws Exception {
        URL url = new URL(requestUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
        writer.write(payload);
        writer.close();

        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuffer jsonString = new StringBuffer();

        String line;
        while ((line = br.readLine()) != null) {
            jsonString.append(line);
        }
        br.close();

        connection.disconnect();

        return jsonString.toString();
    }
}
